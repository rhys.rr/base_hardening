#Configures securtty
class base_hardening::login_defs inherits base_hardening {

  # prepare all variables
  $additional_user_paths = join($base_hardening::extra_user_paths, ':')

  # convert bool to yes/no
  $usergroups_yn = bool2str($base_hardening::usergroups, 'yes', 'no')

  # set the files
  file { '/etc/login.defs':
      ensure  => file,
      content => template('base_hardening/etc/login.defs.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  file { '/etc/bashrc':
      ensure  => file,
      content => template('base_hardening/etc/bashrc.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  file { '/etc/profile':
      ensure  => file,
      content => template('base_hardening/etc/profile.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  file { '/etc/default/useradd':
      ensure  => file,
      content => template('base_hardening/etc/default/useradd.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  file { '/etc/security/pwquality.conf':
      ensure  => file,
      content => template('base_hardening/etc/security/pwquality.conf.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  file { '/etc/libuser.conf':
      ensure  => file,
      content => template('base_hardening/etc/libuser.conf.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

  #Disable Crtl ALT Delete Burst
  file { '/etc/systemd/system.conf':
      ensure  => file,
      content => template('base_hardening/etc/systemd/system.conf.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
  }

}
