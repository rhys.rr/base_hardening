#Configures securtty
class base_hardening::securetty inherits base_hardening {

  $ttys = join($base_hardening::root_ttys, "\n")
  file { '/etc/securetty':
    ensure  => file,
    content => template('base_hardening/etc/securetty.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
  }
}
