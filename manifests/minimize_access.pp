# Configures profile.conf.
class base_hardening::minimize_access inherits base_hardening {

  # remove write permissions from path folders ($PATH) for all regular users
  # this prevents changing any system-wide command from normal users
  ensure_resources ('file',
  { $base_hardening::folders_to_restrict => {
      ensure       => directory,
      links        => follow,
      mode         => 'go-w',
      recurse      => true,
      recurselimit => $base_hardening::recurselimit,
    }
  })

  # shadow must only be accessible to user root
  file { $base_hardening::shadow_path:
    ensure => file,
    owner  => 'root',
    group  => $base_hardening::shadowgroup,
    mode   => $base_hardening::shadowmode,
  }

  # ensure root owns the below files
  file { $base_hardening::root_owned_files:
    owner => 'root',
    group => 'root',
  }

  # su must only be accessible to user and group root
  if $base_hardening::allow_change_user == false {
    file { '/bin/su':
      ensure => file,
      links  => follow,
      owner  => 'root',
      group  => 'root',
      mode   => '0750',
    }
  } else {
    file { '/bin/su':
      ensure => file,
      links  => follow,
      owner  => 'root',
      group  => 'root',
      mode   => '4755',
    }
  }

  # retrieve system users through custom fact
  $system_users = split($::retrieve_system_users, ',')

  # build array of usernames we need to verify/change
  $ignore_users_arr = union($base_hardening::always_ignore_users, $base_hardening::ignore_users)

  # build a target array with usernames to verify/change
  $target_system_users = difference($system_users, $ignore_users_arr)

  # ensure accounts are locked (no password) and use nologin shell
  user { $target_system_users:
    ensure   => present,
    shell    => $base_hardening::nologin_path,
    password => '*',
  }

}
