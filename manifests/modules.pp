#Module Configuration, disabling some modules from loading
class base_hardening::modules inherits base_hardening {

  file {'/etc/modprobe.d':
    ensure  => 'directory',
    source  => 'puppet:///modules/base_hardening/etc/modprobe.d',
    recurse => 'remote',
    path    => '/etc/modprobe.d',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

}
