#Disable Swap
class base_hardening::swap inherits base_hardening {

  if $base_hardening::swap_enabled == false {
    mount { 'swap':
      ensure  => 'absent'
    }

    exec { 'Detach swap file':
      command => '/sbin/swapoff -a',
      onlyif  => '/sbin/swapon -s | /bin/grep -E \'[0-9]\'',
    }
  }

}
