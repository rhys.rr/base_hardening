#Parameters
class base_hardening::params {

  #Prelinking Enabled
  $prelinking_enabled = false

  #where root can log in from
  $root_ttys = ['console','tty1','tty2','tty3','tty4','tty5','tty6']

  #Set core Dump settings
  $enable_core_dump = false

  #Enable 3rd Party openscap repository (https://copr.fedorainfracloud.org/coprs/openscapmaint/openscap-latest/)
  $enable_custom_openscap_repo = true

  #user settings
  $extra_user_paths         = []
  $umask                    = '077'
  $maildir                  = '/var/spool/mail'
  $usergroups               = true
  $sys_uid_min              = 201
  $sys_gid_min              = 201

  $user_default_group_id    = 100
  $user_inactive_after      = 30
  $user_default_shell       = '/bin/bash'
  $user_deault_home         = '/home'
  $user_create_mailspool    = 'yes'
  $user_default_skel        = '/etc/skel'
  $user_default_expire      = ''
  $max_concurrent_logons    = '3'

  $login_retries            = 5
  $login_timeout            = 60
  $chfn_restrict            = ''
  $allow_login_without_home = false

  $bashrc_umask             = '027'
  $profile_umask            = '027'

  #Password settings
  $password_max_age         = 90
  $password_min_age         = 7
  $password_warn_age        = 7
  $password_min_len         = 14
  $password_min_classes     = 3
  $password_min_diff        = 6
  $password_non_reuse       = 5
  $password_special_ch      = '-2'
  $pam_deny                 = 5
  $pam_unlock_time          = 900
  $pam_fail_interval        = 900

  #network settings
  $network_zeroconf_disabled = true

  #Minimize Access
  $allow_change_user   = false
  $always_ignore_users = ['root','sync','shutdown','halt']
  $ignore_users        = ['adminuser']
  $folders_to_restrict = ['/usr/local/games','/usr/local/sbin','/usr/local/bin','/usr/bin','/usr/sbin','/sbin','/bin']
  $shadowgroup         = 'root'
  $shadowmode          = '0000'
  $recurselimit        = 5
  $nologin_path        = '/sbin/nologin'
  $shadow_path         = ['/etc/shadow', '/etc/gshadow']
  $root_owned_files    = ['/etc/passwd', '/etc/group']

  #suid and sgid
  $whitelist           = []
  $blacklist           = []
  $remove_from_unknown = false
  $dry_run_on_unknown  = false

  #Swap 
  $swap_enabled        = true
}
