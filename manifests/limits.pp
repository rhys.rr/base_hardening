#Set the limits file
class base_hardening::limits inherits base_hardening {

  if $base_hardening::enable_core_dump == false {
    file { '/etc/security/limits.d/10.hardening.conf':
      ensure  => file,
      content => template('base_hardening/etc/security/limits.d/10.hardening.conf.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0400',
    }
  } else {
    file { '/etc/security/limits.d/10.hardening.conf':
      ensure => absent,
    }
  }
}
