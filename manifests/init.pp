#Set base CENTOS hardening
class base_hardening (

  $password_max_age            = $base_hardening::params::password_max_age,
  $password_min_age            = $base_hardening::params::password_min_age,
  $password_min_len            = $base_hardening::params::password_min_len,
  $password_warn_age           = $base_hardening::params::password_warn_age,
  $password_min_classes        = $base_hardening::params::password_min_classes,
  $password_min_diff           = $base_hardening::params::password_min_diff,
  $password_non_reuse          = $base_hardening::params::password_non_reuse,
  $root_ttys                   = $base_hardening::params::root_ttys,
  $enable_core_dump            = $base_hardening::params::enable_core_dump,
  $enable_custom_openscap_repo = $base_hardening::params::enable_custom_openscap_repo,
  $user_default_group_id       = $base_hardening::params::user_default_group_id,
  $user_inactive_after         = $base_hardening::params::user_inactive_after,
  $user_default_shell          = $base_hardening::params::user_default_shell,
  $user_deault_home            = $base_hardening::params::user_deault_home,
  $user_create_mailspool       = $base_hardening::params::user_create_mailspool,
  $user_default_skel           = $base_hardening::params::user_default_skel,
  $user_default_expire         = $base_hardening::params::user_default_expire,
  $extra_user_paths            = $base_hardening::params::extra_user_paths,
  $umask                       = $base_hardening::params::umask,
  $maildir                     = $base_hardening::params::maildir,
  $usergroups                  = $base_hardening::params::usergroups,
  $sys_uid_min                 = $base_hardening::params::sys_uid_min,
  $sys_gid_min                 = $base_hardening::params::sys_gid_min,
  $login_retries               = $base_hardening::params::login_retries,
  $login_timeout               = $base_hardening::params::login_timeout,
  $chfn_restrict               = $base_hardening::params::chfn_restrict,
  $allow_login_without_home    = $base_hardening::params::allow_login_without_home,
  $network_zeroconf_disabled   = $base_hardening::params::network_zeroconf_disabled,
  $allow_change_user           = $base_hardening::params::allow_change_user,
  $always_ignore_users         = $base_hardening::params::always_ignore_users,
  $ignore_users                = $base_hardening::params::ignore_users,
  $folders_to_restrict         = $base_hardening::params::folders_to_restrict,
  $shadowgroup                 = $base_hardening::params::shadowgroup,
  $shadowmode                  = $base_hardening::params::shadowmode,
  $recurselimit                = $base_hardening::params::recurselimit,
  $nologin_path                = $base_hardening::params::nologin_path,
  $shadow_path                 = $base_hardening::params::shadow_path,
  $whitelist                   = $base_hardening::params::whitelist,
  $blacklist                   = $base_hardening::params::blacklist,
  $remove_from_unknown         = $base_hardening::params::remove_from_unknown,
  $dry_run_on_unknown          = $base_hardening::params::dry_run_on_unknown,
  $root_owned_files            = $base_hardening::params::root_owned_files,
  $bashrc_umask                = $base_hardening::params::bashrc_umask,
  $profile_umask               = $base_hardening::params::profile_umask,
  $pam_deny                    = $base_hardening::params::pam_deny,
  $pam_unlock_time             = $base_hardening::params::pam_unlock_time,
  $pam_fail_interval           = $base_hardening::params::pam_fail_interval,
  $max_concurrent_logons       = $base_hardening::params::max_concurrent_logons,
  $password_special_ch         = $base_hardening::params::password_special_ch,
  $swap_enabled                = $base_hardening::params::swap_enabled

) inherits base_hardening::params {

  validate_integer($password_max_age)
  validate_integer($password_min_age)
  validate_integer($password_min_len)
  validate_integer($password_warn_age)
  validate_integer($password_min_classes)
  validate_integer($password_min_diff)
  validate_integer($password_non_reuse)
  validate_array($root_ttys)
  validate_bool($enable_core_dump)
  validate_bool($enable_custom_openscap_repo)
  validate_integer($user_default_group_id)
  validate_integer($user_inactive_after)
  validate_string($user_default_shell)
  validate_string($user_deault_home)
  validate_string($user_create_mailspool)
  validate_string($user_default_skel)
  validate_string($user_default_expire)
  validate_array($extra_user_paths)
  validate_string($umask)
  validate_string($maildir)
  validate_bool($usergroups)
  validate_integer($sys_uid_min)
  validate_integer($sys_gid_min)
  validate_integer($login_retries)
  validate_integer($login_timeout)
  validate_string($chfn_restrict)
  validate_bool($allow_login_without_home)
  validate_bool($network_zeroconf_disabled)
  validate_bool($allow_change_user)
  validate_array($always_ignore_users)
  validate_array($ignore_users)
  validate_array($folders_to_restrict)
  validate_string($shadowgroup)
  validate_string($shadowmode)
  validate_integer($recurselimit)
  validate_string($nologin_path)
  validate_array($shadow_path)
  validate_array($whitelist)
  validate_array($blacklist)
  validate_bool($remove_from_unknown)
  validate_bool($dry_run_on_unknown)
  validate_array($root_owned_files)
  validate_string($bashrc_umask)
  validate_string($profile_umask)
  validate_integer($pam_deny)
  validate_integer($pam_unlock_time)
  validate_integer($pam_fail_interval)
  validate_integer($max_concurrent_logons)
  validate_string($password_special_ch)
  validate_bool($swap_enabled)

  #Anchor Class
  anchor { 'base_hardening::begin': }
  -> class { '::base_hardening::openscap': }
  -> class { '::base_hardening::prelink': }
  -> class { '::base_hardening::modules': }
  -> class { '::base_hardening::securetty': }
  -> class { '::base_hardening::limits': }
  -> class { '::base_hardening::profiles': }
  -> class { '::base_hardening::login_defs': }
  -> class { '::base_hardening::network': }
  -> class { '::base_hardening::pam': }
  -> class { '::base_hardening::minimize_access': }
  -> class { '::base_hardening::squid_sgid': }
  -> class { '::base_hardening::service': }
  -> class { '::base_hardening::swap': }
  -> anchor { 'base_hardening::end': }
}
