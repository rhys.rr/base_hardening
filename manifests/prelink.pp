#Set prelinking state
class base_hardening::prelink inherits base_hardening {

  $prelinking_enabled_yn = bool2str($base_hardening::prelinking_enabled, 'yes', 'no')
  $prelink_cmd     = '/usr/sbin/prelink'
  $prelink_disable = 'ua'
  $prelink_enable  = 'a'

  if $base_hardening::prelinking_enabled {
    $prelink_arg = $prelink_enable
  } else {
    $prelink_arg = $prelink_disable
  }

  file { '/etc/sysconfig/prelink':
      ensure  => file,
      content => template('base_hardening/etc/sysconfig/prelink.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      notify  => Exec['Prelink Libaries base_hardening::prelink'],
  }

  exec { 'Prelink Libaries base_hardening::prelink':
    command     => "${prelink_cmd} -${prelink_arg}",
    refreshonly => true,
  }

}
