#OpenScap
class base_hardening::openscap inherits base_hardening {

if $base_hardening::enable_custom_openscap_repo == true {
  file { '/etc/yum.repos.d/openscapmaint-openscap-latest-epel-7.repo':
    ensure  => present,
    content => template('base_hardening/etc/yum.repos.d/openscapmaint-openscap-latest-epel-7.repo.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }
}

}
