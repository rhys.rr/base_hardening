#Manage some services
class base_hardening::service inherits base_hardening {

  #Disable KDump Kernel Crash Analyzer (kdump)
  service { 'kdump.service':
      ensure => 'stopped',
      enable => false,
  }

  #Disable Ctrl-Alt-Del Reboot Activation
  service { 'ctrl-alt-del.target':
      enable => 'mask',
  }

}
