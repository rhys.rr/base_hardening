#
class base_hardening::network inherits base_hardening {

    # convert bool to yes/no
    $network_zeroconf_enabled_yn = bool2str($base_hardening::network_zeroconf_disabled, 'yes', 'no')

    file { '/etc/sysconfig/network':
      ensure  => file,
      content => template('base_hardening/etc/sysconfig/network.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
    }

}
