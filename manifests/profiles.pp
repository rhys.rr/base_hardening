#Configures securtty
class base_hardening::profiles inherits base_hardening {

  if $base_hardening::enable_core_dump == false {
    file { '/etc/profile.d/pinerolo_profile.sh':
      ensure  => file,
      content => template('base_hardening/etc/profile.d/pinerolo_profile.sh.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
    }
  } else {
    file { '/etc/profile.d/pinerolo_profile.sh':
      ensure => absent,
    }
  }
}
