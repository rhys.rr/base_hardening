#Set the pam configuration
class base_hardening::pam inherits base_hardening {

  exec { 'Link system-auth base_hardening::pam':
    command     => '/bin/ln -sf /etc/pam.d/system-auth-local /etc/pam.d/system-auth',
    refreshonly => true,
  }

  exec { 'Link password-auth base_hardening::pam':
    command     => '/bin/ln -sf /etc/pam.d/password-auth-local /etc/pam.d/password-auth',
    refreshonly => true,
  }

  exec { 'Link smartcard-auth base_hardening::pam':
    command     => '/bin/ln -sf /etc/pam.d/smartcard-auth-local /etc/pam.d/smartcard-auth',
    refreshonly => true,
  }

  service { 'pcscd.socket':
    ensure => 'running',
  }

  file { '/etc/pam.d/password-auth-local':
      ensure  => file,
      content => template('base_hardening/etc/pam.d/password-auth-local.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      notify  => Exec[ 'Link password-auth base_hardening::pam' ],
  }

  file { '/etc/pam.d/system-auth-local':
      ensure  => file,
      content => template('base_hardening/etc/pam.d/system-auth-local.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      notify  => Exec[ 'Link system-auth base_hardening::pam' ],
  }

  file { '/etc/pam.d/smartcard-auth-local':
      ensure  => file,
      content => template('base_hardening/etc/pam.d/smartcard-auth-local.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      notify  => Exec[ 'Link smartcard-auth base_hardening::pam' ],
  }

  file { '/etc/pam_pkcs11/pam_pkcs11.conf':
      ensure  => file,
      content => template('base_hardening/etc/pam_pkcs11/pam_pkcs11.conf.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      notify  => Service['pcscd.socket'],
  }

}

